﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExeliaAPITest.Models
{
    [Table("RatingDetail")]
    public class RatingDetail
    {
        [Key]
        public int RatingId { get; set; }
        public decimal? Rating { get; set; }

        [ForeignKey("BeerId")]
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
    }
}