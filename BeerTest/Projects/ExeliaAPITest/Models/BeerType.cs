﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExeliaAPITest.Models
{
    [Table("BeerType")]
    public class BeerType
    {
        [Key]
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}