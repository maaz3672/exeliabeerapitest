﻿using ExeliaAPITest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;


namespace ExeliaAPITest.Controllers

{
   
    [RoutePrefix("api/Beers")]
    public class BeerController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("AllBeers")]
        [HttpGet]
        public IEnumerable<RatingAverage> AllBeers()
        {
            
            return db.RatingAverages.Include(x => x.BeerType).ToList();
        }

        [Route("BeerTypes")]
        [HttpGet]
        public IEnumerable<BeerType> BeerTypes()
        {

            return db.BeerTypes.ToList();
        }

        [Route("AddBeer")]
        [HttpPost]
        public ResponseMessage AddBeer(Beer beer)
        {

            ResponseMessage resposne = new ResponseMessage();
            try
            {

                db.Beers.Add(beer);
                db.SaveChanges();
                if (beer.AverageRating > 0)
                {
                    RatingDetail rating = new RatingDetail();
                    rating.Rating = beer.AverageRating;
                    rating.BeerId = beer.BeerId;
                    db.RatingDetails.Add(rating);
                    db.SaveChanges();
                }
                resposne.code = "00";
                resposne.message = "Record saved successfully.";
            }

            catch (Exception ex)
            {
                resposne.code = "500";
                resposne.message ="Exception occured. "+ex.Message;
            }


            return resposne;
        }

        [Route("AddBeerType")]
        [HttpPost]
        public ResponseMessage AddBeerType(BeerType type)
        {

            ResponseMessage resposne = new ResponseMessage();
            try
            {

                db.BeerTypes.Add(type);
                db.SaveChanges();
                resposne.code = "00";
                resposne.message = "Record saved successfully.";
            }

            catch (Exception ex)
            {
                resposne.code = "500";
                resposne.message = "Exception occured. " + ex.Message;
            }


            return resposne;
        }
        [Route("AddBeerRating")]
        [HttpPost]
        public ResponseMessage AddBeerRating(RatingDetail type)
        {

            ResponseMessage resposne = new ResponseMessage();
            try
            {

                db.RatingDetails.Add(type);
                db.SaveChanges();
                resposne.code = "00";
                resposne.message = "Record saved successfully.";
            }

            catch (Exception ex)
            {
                resposne.code = "500";
                resposne.message = "Exception occured. " + ex.Message;
            }


            return resposne;
        }
    }
}
