﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExeliaAPITest.Models
{
    [Table("RatingAverage")]
    public class RatingAverage
    {
        [Key]
        public int BeerId { get; set; }
        public String BeerName { get; set; }
        public String BeerBrand { get; set; }
        [ForeignKey("TypeId")]
        public BeerType BeerType { get; set; }
        public int TypeId { get; set; }
        public decimal? AverageRating { get; set; }
    }
}