﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExeliaAPITest.Models
{
    [Table("Beer")]
    public class Beer
    {
        [Key]
        public int BeerId { get; set; }
        public String BeerName { get; set; }
        public String BeerBrand { get; set; }

        [ForeignKey("TypeId")]
        public BeerType BeerType { get; set; }
        public int TypeId { get; set; }
        [NotMapped]
        public int? AverageRating { get; set; }
        [NotMapped]
        public decimal? Average { get; set; }
       
        
    }
}