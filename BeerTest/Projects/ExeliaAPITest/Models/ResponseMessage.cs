﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExeliaAPITest.Models
{
    public class ResponseMessage
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}